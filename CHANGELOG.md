# v2 - September 2018
* Fixed errors in comments.
* Adjusted rates for Helix, Claw, & Root Fossils.
* Fixed rate for Odd Keystone.
* Updated for E18.

# v1 - March 2018
* Initial release.