![Header image for Mining Overhaul readme](https://media.ariastudio.dev/system/my/mining-overhaul.png)

# Mining Overhaul
This mod does a few different things, but its primary purpose is to add the evolution stones (from Gen IV forward) and the fossils (from Gen V forward) to the mining minigame in [Pokémon Reborn](https://rebornevo.com). However, while I was doing that, I added in three other valuable (selling value) items as well! I also went a step further and overhauled the probability values of the mining system, making it a smidge easier to acquire fossils and other useful items. The last thing I did for the purposes of this mod was to create two new "delimiters" for two particular categories of items which will ensure that you can only get one of those per mining round. This mod uses the modular setup that was adopted into the core Reborn code, which makes this mod super easy to install and also fairly compatible with other mods.

## New Items Available via Mining
* Fossils
  * Cover Fossil
  * Plume Fossil
  * Sail Fossil
  * Jaw Fossil
* Evolutionary Stones
  * Dawn Stone
  * Dusk Stone
  * Shiny Stone
  * Ice Stone
* Valuable Items
  * Nugget
  * Big Nugget
  * Comet Shard

## Mining Probabilities & Delimitation
The short and quick version is as follows:
* Fossils = 4.20% chance to find*
* Plates = 14.28% chance to find*
* Rare Miscellaneous = 6.3% chance to find*
* Evolutionary Stones = 16.70% chance to find*
* Uncommon Miscellaneous = 16.72% chance to find
* Common Miscellaneous = 12.55% chance to find
* Shards & Heart Scales = 29.30% chance to find

Delimitation, which is just a fancy way of saying that you can only find one item from each category per mining screen. Mixing and matching is possible though! So, for example: you could only find a Dusk Stone **or** a Fire Stone, but you could find a Sail Fossil **and** a Meadow Plate simultaneously. For more information on the probabilities for each item, including a listing of the categories, [see this spreadsheet](https://myas.link/mo-probabilities). That spreadsheet also lists the vanilla Reborn values so that you can easily compare and contrast the two.

# Installation
1. Download the mod from any of the available sources in the download section.
1. Unzip the folder.
1. Go to where you unzipped the downloaded folder, then grab the entire “Mods” folder and place it within the “Data” folder of your Pokémon Reborn installation. If you already have some mods installed, you might have to merge the folders.

# Frequently Asked Questions
> Why didn't you add the ability to mine Old Amber?

I felt that went too far against the flow Ame intended for the game. The Cover & Plume Fossils are obtainable almost immediately at the same time as the ability to revive them is unlocked. However, by adding the Sail & Jaw Fossils, they are now effectively available almost three gyms early! The Old Amber is simply too far back in the game to justify inclusion; however, I might reconsider adding it at a later date.

> Where did the artwork come from?

The artwork for the Fossils and the Evolutionary Stones are their Dream World/Global Link/”official” artwork which were all edited by myself to match the existing artstyle for mining at least marginally better. The valuable items are all edited versions (again by me) of other mining items. I am most certainly not an artist, so these were the best I could do. However, if anyone else would like to offer better alternatives I will certainly consider replacing them.

> Is this mod compatible with other mods?

The short answer is yes! Previously we relied on adding the code that enabled the modular setup ourselves; however, this code is now present in the core Reborn download already! As long as another mod doesn’t remove this necessary bit of code, then this mod should be compatible with pretty much any others. No guarantees are made however. If you have issues, please take a look at the help tab above.

# Downloads
Downloads for this project can be found [here on GitLab](https://gitlab.com/ariastudios/mining-overhaul/-/releases) or [on our website](https://myas.link/mining-overhaul).

# Help
You can post any issues or questions about the mod on [its thread on the Reborn Evolved forum here](https://myas.link/mo-thread).

# Credits
* Maruno – original creator of the mining minigame script.
* Amethyst & the rest of the dev team – creator and developers for the game overall & adding the modular code to the core Reborn game.
* Bazaro – creator of the Amplified Rock artwork/sprite which is carried over from the original Reborn mining graphic.
* Waynolt & Aironfaar – the modular setup originally used for this mod.
